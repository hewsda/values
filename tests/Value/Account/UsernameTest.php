<?php

declare(strict_types=1);

namespace ValuesTests\Value\Account;

use Hewsda\Values\Value\Account\Username;
use ValuesTests\TestCase;

class UsernameTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_generated_from_string()
    {
        $n = Username::fromString('username');

        $this->assertInstanceOf(Username::class, $n);
    }

    /**
     * @test
     * @expectedException \Hewsda\Values\Exception\ValueObjectException
     * @dataProvider getWrongConstraints
     * @param string $username
     */
    public function it_raise_an_exception_with_constraints(string $username)
    {
        $n = Username::fromString($username);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $n = Username::fromString('username');

        $this->assertTrue($n->sameValueAs($n));
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $username = 'username';
        $p = Username::fromString($username);

        $this->assertEquals($username, $p->toString());
        $this->assertEquals($username, (string)$p);
    }

    public function getWrongConstraints()
    {
        return [
            [''], // empty
            [str_random(Username::MIN - 1)], // min car
            [str_random(Username::MAX + 1)], // max car
        ];
    }
}