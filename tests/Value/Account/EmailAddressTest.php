<?php

declare(strict_types=1);

namespace ValuesTests\Value\Account;

use Hewsda\Values\Value\Account\EmailAddress;
use ValuesTests\TestCase;

class EmailAddressTest extends TestCase
{
    private $em;

    public function setUp()
    {
        $this->em = $this->getMockBuilder(EmailAddress::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function it_can_be_initialized()
    {
        $e = EmailAddress::fromString('some@email.com');

        $this->assertInstanceOf(EmailAddress::class, $e);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $email = 'some@email.com';
        $e = EmailAddress::fromString($email);

        $this->em->expects($this->once())
            ->method('toString')
            ->willReturn($email);

        $this->assertTrue($e->sameValueAs($this->em));
    }

    /**
     * @test
     * @expectedException \Hewsda\Values\Exception\ValueObjectException
     * @dataProvider getArgs
     * @param mixed $arg
     */
    public function it_raise_exception_with_wrong_email($arg)
    {
        $e = EmailAddress::fromString($arg);
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $email = 'some@email.com';
        $e = EmailAddress::fromString($email);

        $this->assertEquals($email, $e->toString());
        $this->assertEquals($email, (string)$e);
    }

    public function getArgs()
    {
        return [
            [''], ['email'], [0]
        ];
    }
}