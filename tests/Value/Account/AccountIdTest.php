<?php

declare(strict_types=1);

namespace ValuesTests\Value\Account;

use Hewsda\Values\Value\Account\AccountId;
use Ramsey\Uuid\UuidInterface;
use ValuesTests\Mock\SomeIdentifier;
use ValuesTests\TestCase;

class AccountIdTest extends TestCase
{
    private $uid;

    public function setUp()
    {
        $this->uid = new SomeIdentifier;
    }

    /**
     * @test
     */
    public function it_can_be_generated()
    {
        $m = AccountId::make();

        $this->assertInstanceOf(AccountId::class, $m);
        $this->assertInstanceOf(UuidInterface::class, $m->identify());
    }

    /**
     * @test
     */
    public function it_can_be_generated_from_string()
    {
        $p = AccountId::fromString($this->uid->toString());

        $this->assertInstanceOf(AccountId::class, $p);
    }

    /**
     * @test
     * @expectedException \Hewsda\Values\Exception\ValueObjectException
     * @dataProvider getArgs
     * @param mixed $arg
     */
    public function it_raise_exception_when_generated_from_invalid_uid_string($arg)
    {
        $p = AccountId::fromString($arg);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $p = AccountId::fromString($this->uid->toString());
        $p1 = AccountId::fromString($this->uid->toString());

        $this->assertTrue($p->sameValueAs($p1));
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $p = AccountId::fromString($this->uid->toString());

        $this->assertEquals($p->toString(), $this->uid->toString());
        $this->assertEquals((string)$p, $this->uid->toString());
    }

    public function getArgs()
    {
        return [
            [''], ['not_valid']
        ];
    }
}