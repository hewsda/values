<?php

declare(strict_types=1);

namespace ValuesTests\Value;

use Hewsda\Values\Value\ValueObject;
use ValuesTests\TestCase;

class ValueObjectTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_check_value_equality()
    {
        $v = new SomeValueObject(true);

        $this->assertTrue($v->sameValueAs($v));

        $v1 = new SomeValueObject(false);

        $this->assertFalse($v1->sameValueAs($v));
    }
}

class SomeValueObject implements ValueObject
{
    private $value;

    public function __construct(bool $value)
    {
        $this->value = $value;
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this && $this->getValue() === $aObject->getValue();
    }

    public function getValue(): bool
    {
        return $this->value;
    }
}