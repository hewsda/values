<?php

declare(strict_types=1);

namespace Value\Person;

use Hewsda\Values\Value\Person\LastName;
use Hewsda\Values\Value\Person\Name;
use ValuesTests\TestCase;

class LastNameTest extends TestCase
{
    private $someName = 'white';
    private $lastName;

    public function setUp()
    {
        $this->lastName = LastName::fromString($this->someName);
    }

    /**
     * @test
     */
    public function it_can_be_initialized_from_string()
    {
        $this->assertInstanceOf(LastName::class, $this->lastName);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $this->assertTrue($this->lastName->sameValueAs($this->lastName));
    }

    /**
     * @test
     */
    public function it_can_access_name_instance()
    {
        $this->assertInstanceOf(Name::class, $this->lastName->getName());
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $this->assertEquals($this->someName, (string)$this->lastName);
        $this->assertEquals($this->someName, $this->lastName->toString());
    }
}