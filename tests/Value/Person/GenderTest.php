<?php

declare(strict_types=1);

namespace ValuesTests\Value\Person;

use Hewsda\Values\Enum\Enum;
use Hewsda\Values\Value\Person\Gender;
use ValuesTests\TestCase;

class GenderTest extends TestCase
{
    /**
     * @test
     */
    public function it_instantiate_gender()
    {
        $g = $this->getGender();

        $this->assertInstanceOf(Gender::class, $g);
        $this->assertInstanceOf(Enum::class, $g);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $g = $this->getGender();
        $this->assertTrue($g->sameValueAs($g));
    }

    /**
     * @test
     * @dataProvider  getGenders
     * @param string $gender
     */
    public function it_check_every_gender(string $gender)
    {
        $this->assertTrue(Gender::has($gender), sprintf('Gender %s is not part of fixture.', $gender));
    }

    public function getGender(): Gender
    {
        return Gender::get('female');
    }

    public function getGenders(): array
    {
        return [
            ['female'], ['male'], ['other']
        ];
    }
}