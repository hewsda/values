<?php

declare(strict_types=1);

namespace ValuesTests\Value\Person;

use Hewsda\Values\Value\Person\FirstName;
use Hewsda\Values\Value\Person\LastName;
use Hewsda\Values\Value\Person\PersonName;
use ValuesTests\TestCase;

class PersonNameTest extends TestCase
{
    private $first;
    private $last;

    public function setUp()
    {
        $this->first = $this->getMockBuilder(FirstName::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->last = $this->getMockBuilder(LastName::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function it_can_be_constructed()
    {
        $p = new PersonName($this->first, $this->last);

        $this->assertInstanceOf(PersonName::class, $p);
        $this->assertInstanceOf(FirstName::class, $p->getFirstName());
        $this->assertInstanceOf(LastName::class, $p->getLastName());
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $this->first->expects($this->once())
            ->method('sameValueAs')
            ->willReturn(true);

        $this->last->expects($this->once())
            ->method('sameValueAs')
            ->willReturn(true);

        $p = new PersonName($this->first, $this->last);

        $this->assertTrue($p->sameValueAs($p));
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $this->first->expects($this->once())
            ->method('toString')
            ->willReturn('walter');

        $this->last->expects($this->once())
            ->method('toString')
            ->willReturn('white');

        $p = new PersonName($this->first, $this->last);

        $this->assertEquals('walter white', (string)$p);
    }
}