<?php

declare(strict_types=1);

namespace ValuesTests\Value\Person;

use Hewsda\Values\Value\Person\Gender;
use Hewsda\Values\Value\Person\Person;
use Hewsda\Values\Value\Person\PersonId;
use Hewsda\Values\Value\Person\PersonName;
use ValuesTests\TestCase;

class PersonTest extends TestCase
{
    private $id;
    private $name;
    private $gender;

    public function setUp()
    {
        $this->id = $this->getMockBuilder(PersonId::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->name = $this->getMockBuilder(PersonName::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->gender = $this->getMockBuilder(Gender::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $this->id->expects($this->once())
            ->method('sameValueAs')
            ->willReturn(true);

        $this->name->expects($this->once())
            ->method('sameValueAs')
            ->willReturn(true);

        $this->gender->expects($this->once())
            ->method('sameValueAs')
            ->willReturn(true);

        $p = new Person($this->id, $this->name, $this->gender);

        $this->assertTrue($p->sameValueAs($p));
    }

    /**
     * @test
     */
    public function it_can_be_transform_to_array()
    {
        $id = 1;
        $name = 'walter white';
        $gender = 'male';

        $this->id->expects($this->once())
            ->method('toString')
            ->willReturn($id);

        $this->name->expects($this->once())
            ->method('toString')
            ->willReturn($name);

        $this->gender->expects($this->once())
            ->method('toString')
            ->willReturn($gender);

        $p = new Person($this->id, $this->name, $this->gender);

        $this->assertEquals([
            'personId' => $id,
            'personName' => $name,
            'personGender' => $gender
        ], $p->toArray());
    }
}