<?php

declare(strict_types=1);

namespace ValuesTests\Value\Person;

use Hewsda\Values\Value\Person\Name;
use ValuesTests\TestCase;

class NameTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_generated_from_string()
    {
        $n = Name::fromString('john');

        $this->assertInstanceOf(Name::class, $n);
    }

    /**
     * @test
     * @expectedException \Hewsda\Values\Exception\ValueObjectException
     * @dataProvider getWrongConstraints
     * @param string $name
     */
    public function it_raise_an_exception_with_constraints(string $name)
    {
        $n = Name::fromString($name);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $n = Name::fromString('walter');

        $this->assertTrue($n->sameValueAs($n));
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $name = 'walter';

        $p = Name::fromString($name);

        $this->assertEquals($name, $p->toString());
        $this->assertEquals($name, (string)$p);
    }

    public function getWrongConstraints()
    {
        return [
            [''], // empty
            [str_random(Name::MIN - 1)], // min car
            [str_random(Name::MAX + 1)], // max car
        ];
    }
}