<?php

declare(strict_types=1);

namespace ValuesTests\Value\Person;

use Hewsda\Values\Value\Person\FirstName;
use Hewsda\Values\Value\Person\Name;
use ValuesTests\Mock\SomeFirstName;
use ValuesTests\TestCase;

class FirstNameTest extends TestCase
{
    private $someName = SomeFirstName::FIRSTNAME;
    private $firstName;
    private $name;

    public function setUp()
    {
        $this->firstName = $this->getMockBuilder(FirstName::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->name = $this->getMockBuilder(Name::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function it_can_be_initialized_from_string()
    {
        /** @var FirstName $m */
        $m = (new SomeFirstName)('walter');

        $this->assertInstanceOf(FirstName::class, $m);

        $this->assertEquals('walter', $m->toString());
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $this->name->expects($this->once())
            ->method('toString')
            ->willReturn($this->someName);

        $this->firstName->expects($this->once())
            ->method('getName')
            ->willReturn($this->name);

        $firstName = FirstName::fromString($this->someName);

        $this->assertTrue($firstName->sameValueAs($this->firstName));
    }

    /**
     * @test
     */
    public function it_can_access_name_instance()
    {
        $this->assertInstanceOf(Name::class, $this->firstName->getName());
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $this->firstName->expects($this->once())
            ->method('toString')
            ->willreturn($this->someName);

        $this->firstName->expects($this->exactly(2))
            ->method('__toString')
            ->willreturn($this->someName);

        $this->assertEquals($this->someName, $this->firstName->toString());

        $this->assertEquals($this->someName, (string)$this->firstName);

        $this->assertEquals($this->someName, $this->firstName->__toString());
    }
}