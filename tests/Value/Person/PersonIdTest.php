<?php

declare(strict_types=1);

namespace ValuesTests\Value\Person;

use Hewsda\Values\Value\Person\PersonId;
use Ramsey\Uuid\UuidInterface;
use ValuesTests\Mock\SomeIdentifier;
use ValuesTests\TestCase;

class PersonIdTest extends TestCase
{
    private $uid;

    public function setUp()
    {
        $this->uid = new SomeIdentifier;
    }

    /**
     * @test
     */
    public function it_can_be_generated()
    {
        $m = PersonId::make();

        $this->assertInstanceOf(PersonId::class, $m);
        $this->assertInstanceOf(UuidInterface::class, $m->identify());
    }

    /**
     * @test
     */
    public function it_can_be_generated_from_string()
    {
        $p = PersonId::fromString($this->uid->toString());

        $this->assertInstanceOf(PersonId::class, $p);
    }

    /**
     * @test
     * @expectedException \Hewsda\Values\Exception\ValueObjectException
     * @dataProvider getArgs
     * @param mixed $arg
     */
    public function it_raise_exception_when_generated_from_invalid_uid_string($arg)
    {
        $p = PersonId::fromString($arg);
    }

    /**
     * @test
     */
    public function it_can_be_compared()
    {
        $p = PersonId::fromString($this->uid->toString());
        $p1 = PersonId::fromString($this->uid->toString());

        $this->assertTrue($p->sameValueAs($p1));
    }

    /**
     * @test
     */
    public function it_can_be_serialized()
    {
        $p = PersonId::fromString($this->uid->toString());

        $this->assertEquals($p->toString(), $this->uid->toString());
        $this->assertEquals((string)$p, $this->uid->toString());
    }

    public function getArgs()
    {
        return [
            [''], ['0'], ['not_valid']
        ];
    }
}