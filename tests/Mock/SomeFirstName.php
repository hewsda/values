<?php

declare(strict_types=1);

namespace ValuesTests\Mock;

use Hewsda\Values\Value\Person\FirstName;

class SomeFirstName
{
    const FIRSTNAME = 'walter';

    public function __invoke(string $name = null): FirstName
    {
        return FirstName::fromString($name ?? self::FIRSTNAME);
    }
}
