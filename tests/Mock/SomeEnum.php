<?php

declare(strict_types=1);

namespace ValuesTests\Mock;

use Hewsda\Values\Enum\Enum;

class SomeEnum extends Enum
{
    const VALUE = 'value';
}