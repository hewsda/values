<?php

declare(strict_types=1);

namespace ValuesTests\Mock;

use Ramsey\Uuid\Uuid;

class SomeIdentifier
{
    private $uid;

    public function __construct()
    {
        $this->uid = Uuid::uuid4();
    }

    public function toString()
    {
        return $this->uid->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }

}