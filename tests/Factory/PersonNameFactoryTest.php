<?php

declare(strict_types=1);

namespace ValuesTests\Factory;

use Hewsda\Values\Factory\PersonNameFactory;
use Hewsda\Values\Value\Person\PersonName;
use ValuesTests\TestCase;

class PersonNameFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_return_a_person_name_instance()
    {
        $p = PersonNameFactory::make('john', 'doe');

        $this->assertInstanceOf(PersonName::class, $p);
    }
}