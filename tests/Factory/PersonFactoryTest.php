<?php

declare(strict_types=1);

namespace ValuesTests\Factory;

use Hewsda\Values\Factory\PersonFactory;
use Hewsda\Values\Value\Person\Person;
use ValuesTests\TestCase;

class PersonFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_return_a_person_instance()
    {
        $p = PersonFactory::make('john', 'doe', 'male');

        $this->assertInstanceOf(Person::class, $p);
    }
}