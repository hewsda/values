<?php

declare(strict_types=1);

namespace Hewsda\Values\Value;

interface ValueObject
{
    public function sameValueAs(ValueObject $aObject): bool;
}