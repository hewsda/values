<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Person;

use Hewsda\Values\Value\ValueObject;

class FirstName implements ValueObject
{
    /**
     * @var Name
     */
    private $name;

    /**
     * FirstName constructor.
     *
     * @param Name $name
     */
    private function __construct(Name $name)
    {
        $this->name = $name;
    }

    public static function fromString(string $name): self
    {
        return new self(Name::fromString($name));
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this && $this->getName()->sameValueAs($aObject->getName());
    }

    public function toString(): string
    {
        return $this->name->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}