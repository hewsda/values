<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Person;

use Hewsda\Values\Enum\Enum;

/**
 * Class Gender
 *
 * @method static Gender FEMALE()
 * @method static Gender MALE()
 * @method static Gender OTHER()
 */
class Gender extends Enum
{
    const FEMALE = 'female';
    const MALE = 'male';
    const OTHER = 'other';
}