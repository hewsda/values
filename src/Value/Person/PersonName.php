<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Person;

use Hewsda\Values\Value\ValueObject;

class PersonName implements ValueObject
{
    /**
     * @var FirstName
     */
    private $firstName;

    /**
     * @var LastName
     */
    private $lastName;

    /**
     * PersonName constructor.
     *
     * @param FirstName $firstName
     * @param LastName $lastName
     */
    public function __construct(FirstName $firstName, LastName $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function sameValueAs(ValueObject $personName): bool
    {
        return
            $personName instanceof $this &&
            $this->firstName->sameValueAs($personName->getFirstName()) &&
            $this->lastName->sameValueAs($personName->getLastName());
    }

    public function toString(): string
    {
        return $this->firstName->toString() . ' ' . $this->lastName->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}