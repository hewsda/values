<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Person;

use Hewsda\Values\Value\ValueObject;

class Person implements ValueObject
{
    /**
     * @var PersonId
     */
    private $personId;

    /**
     * @var PersonName
     */
    private $personName;

    /**
     * @var Gender
     */
    private $gender;

    /**
     * Person constructor.
     *
     * @param PersonId $personId
     * @param PersonName $personName
     * @param Gender $gender
     */
    public function __construct(PersonId $personId, PersonName $personName, Gender $gender)
    {
        $this->personId = $personId;
        $this->personName = $personName;
        $this->gender = $gender;
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this &&
            $this->getId()->sameValueAs($aObject->getId()) &&
            $this->getName()->sameValueAs($aObject->getName()) &&
            $this->getGender()->sameValueAs($aObject->getGender());
    }

    public function getId(): PersonId
    {
        return $this->personId;
    }

    public function getName(): PersonName
    {
        return $this->personName;
    }

    public function getGender(): Gender
    {
        return $this->gender;
    }

    public function toArray(): array
    {
        return [
            'personId' => $this->personId->toString(),
            'personName' => $this->personName->toString(),
            'personGender' => $this->gender->toString()
        ];
    }
}