<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Account;

use Hewsda\Values\Assertion;
use Hewsda\Values\Value\ValueObject;

class EmailAddress implements ValueObject
{
    /**
     * @var string
     */
    private $emailAddress;

    /**
     * EmailAddress constructor.
     *
     * @param string $emailAddress
     */
    private function __construct(string $emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    public static function fromString($emailAddress): self
    {
        Assertion::email($emailAddress);

        return new self($emailAddress);
    }

    public function toString(): string
    {
        return $this->emailAddress;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this && $this->toString() === $aObject->toString();
    }
}