<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Account;

use Hewsda\Values\Assertion;
use Hewsda\Values\Exception\ValueObjectException;
use Hewsda\Values\Value\ValueObject;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AccountId implements ValueObject
{
    /**
     * @var UuidInterface
     */
    private $uid;

    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public static function make(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($uid): self
    {
        Assertion::notEmpty($uid);
        Assertion::string($uid);

        if (Uuid::isValid($uid)) {
            return new self(Uuid::fromString($uid));
        }

        throw new ValueObjectException(
            'Account identifier is not valid.',
            \Assert\Assertion::INVALID_UUID,
            self::class,
            $uid
        );
    }

    public function toString(): string
    {
        return $this->uid->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function identify(): UuidInterface
    {
        return $this->uid;
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this && $this->toString() === $aObject->toString();
    }
}