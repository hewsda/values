<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Account\Activation;

use Hewsda\Values\Value\ValueObject;

class AccountActivation implements ValueObject
{
    /**
     * @var ActivationFlag
     */
    private $flag;

    /**
     * @var \DateTimeImmutable
     */
    private $dateTime;

    /**
     * AccountActivation constructor.
     *
     * @param ActivationFlag $flag
     * @param \DateTimeImmutable $dateTime
     */
    private function __construct(ActivationFlag $flag, \DateTimeImmutable $dateTime)
    {
        $this->flag = $flag;
        $this->dateTime = $dateTime;
    }

    public function getDatetime(): \DateTimeImmutable
    {
        return $this->dateTime;
    }

    public function getFlag(): ActivationFlag
    {
        return $this->flag;
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this &&
            $this->getFlag()->sameValueAs($aObject->getFlag()) &&
            $this->getDatetime() === $aObject->getDatetime();
    }
}