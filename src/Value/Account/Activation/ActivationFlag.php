<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Account\Activation;

use Hewsda\Values\Enum\Enum;

/**
 * Class ActivationFlag
 *
 * @method static ActivationFlag PENDING()
 * @method static ActivationFlag NOTACTIVATED()
 * @method static ActivationFlag ACTIVATED()
 */
class ActivationFlag extends Enum
{
    const PENDING = 0;
    const NOTACTIVATED = 1;
    const ACTIVATED = 2;
}