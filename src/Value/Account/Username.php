<?php

declare(strict_types=1);

namespace Hewsda\Values\Value\Account;

use Hewsda\Values\Assertion;
use Hewsda\Values\Value\ValueObject;

class Username implements ValueObject
{
    const MAX = 30;
    const MIN = 8;

    /**
     * @var string
     */
    private $username;

    /**
     * Name constructor.
     *
     * @param string $username
     */
    private function __construct(string $username)
    {
        $this->username = $username;
    }

    public static function fromString(string $username): self
    {
        Assertion::betweenLength($username, self::MIN, self::MAX);

        return new self($username);
    }

    public function toString(): string
    {
        return $this->username;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function sameValueAs(ValueObject $aObject): bool
    {
        return $aObject instanceof $this && $this->toString() === $aObject->toString();
    }
}