<?php

declare(strict_types=1);

namespace Hewsda\Values\Factory;

use Hewsda\Values\Value\Person\Gender;
use Hewsda\Values\Value\Person\Person;
use Hewsda\Values\Value\Person\PersonId;

class PersonFactory
{
    public static function make($firstName, $lastName, $gender, $personId = null): Person
    {
        return new Person(
            $personId ? PersonId::fromString($personId) : PersonId::make(),
            PersonNameFactory::make($firstName, $lastName),
            Gender::get($gender)
        );
    }
}