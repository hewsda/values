<?php

declare(strict_types=1);

namespace Hewsda\Values\Factory;

use Hewsda\Values\Value\Person\FirstName;
use Hewsda\Values\Value\Person\LastName;
use Hewsda\Values\Value\Person\PersonName;

class PersonNameFactory
{
    public static function make(... $names): PersonName
    {
        [$firstName, $lastName] = $names;

        return new PersonName(
            FirstName::fromString($firstName),
            LastName::fromString($lastName)
        );
    }
}