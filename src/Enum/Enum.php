<?php

declare(strict_types=1);

namespace Hewsda\Values\Enum;

use Hewsda\Values\Value\ValueObject;
use MabeEnum\Enum as EnumBase;

class Enum extends EnumBase implements ValueObject
{

    public function sameValueAs(ValueObject $object): bool
    {
        return $this->is($object);
    }

    public function toString(): string
    {
        return $this->__toString();
    }
}