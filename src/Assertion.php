<?php

declare(strict_types=1);

namespace Hewsda\Values;

use Assert\Assertion as BaseAssertion;

class Assertion extends BaseAssertion
{
    /**
     * @var string
     */
    protected static $exceptionClass = Exception\ValueObjectException::class;
}